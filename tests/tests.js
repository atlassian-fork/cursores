module('Cursores.js');

test('constructor maxLength', function () {
  strictEqual(new Cursores().maxLength, 128, 'unspecified');
  strictEqual(new Cursores(null, null, 0).maxLength, 128, 'zero');
  strictEqual(new Cursores(null, null, -1).maxLength, 128, 'negative');
});

test('constructor regex anchors', function () {
  var cursor = new Cursores(/(foo)/, /(bar)/);
  ok(cursor.leftRegex.test('foo'), 'left at string end');
  ok(!cursor.leftRegex.test('foo '), 'left not at string end');
  ok(cursor.rightRegex.test('bar'), 'right at string end');
  ok(!cursor.rightRegex.test(' bar'), 'right not at string start');
});

test('whitespace delimited token', function () {
  var cursor = new Cursores();
  eqTokenAll(cursor, '|', '', 'empty string');
  eqTokenAll(cursor, '|foo bar', '', 'start of string');
  eqTokenAll(cursor, 'foo bar|', 'bar|', 'end of string');
  eqTokenAll(cursor, 'foo |bar', '', "left edge doesn't match");
  eqTokenAll(cursor, 'foo b|ar', 'b|ar', 'middle of word');
  eqTokenAll(cursor, 'foo bar| quux', 'bar|', 'right edge of word');
});

test('whitespace delimited token starting with character', function () {
  var cursor = Cursores.startsWith('@');
  eqTokenAll(cursor, '|', '', 'empty string');
  eqTokenAll(cursor, '|@foo bar', '', 'start of string');
  eqTokenAll(cursor, 'foo @bar @quux|', '@quux|', 'end of string');
  eqTokenAll(cursor, 'foo |@bar', '', "left edge doesn't match");
  eqTokenAll(cursor, 'foo @b|ar', '@b|ar', 'middle of word');
  eqTokenAll(cursor, 'foo @bar|', '@bar|', 'right edge of word');
  eqTokenAll(cursor, 'f|oo @bar', '', "doesn't match word on left");
  eqTokenAll(cursor, 'foo @bar qu|ux', '', "doesn't match word onright");
});

test('replace cursor movement', function () {
  var cursor = new Cursores();
  eqReplaceAll(cursor, ' f|oo ', '', ' | ', 'empty replacement');
  eqReplaceAll(cursor, ' f|oo ', 'quux', ' quux| ', 'longer word');
  eqReplaceAll(cursor, ' f|oo ', 'f', ' f| ', 'shorter word');
  eqReplaceAll(
    cursor, ' f|oo ', 'bar', ' bar| ', 'same length, cursor moves'
  );
  eqReplaceAll(
    cursor, ' foo| ', 'bar', ' bar| ',
    "same length with cursor at word end, cursor doesn't move"
  );
  eqReplaceAll(
    cursor, ' f|oo ', 'foo', ' foo| ', 'same word, cursor moves'
  );
});

test('no token to replace', function () {
  var cursor = new Cursores();
  eqReplaceAll(cursor, 'foo | bar', 'quux', 'foo | bar', 'between words');
  eqReplaceAll(cursor, 'foo |bar', 'quux', 'foo |bar', 'left edge');
});

test('replace into an empty source', function () {
  var cursor = new Cursores();
  eqReplaceAll(cursor, '|', 'foo', 'foo|', 'replaces in an empty source');
});


module('Test helpers');

function eqTokenAll(cursor, cursorString, tokenCursorString, message) {
  var textarea = document.querySelector('#qunit-fixture textarea');
  type(textarea, cursorString);
  eqToken(
    cursor.token(textarea), tokenCursorString,
    message ? message + ' (textarea)' : undefined
  );

  var input = document.querySelector('#qunit-fixture input');
  type(input, cursorString);
  eqToken(
    cursor.token(input), tokenCursorString,
    message ? message + ' (input)' : undefined
  );

  eqToken(
    cursor.token(parse(cursorString).value, parse(cursorString).index),
    tokenCursorString,
    message ? message + ' (string)' : undefined
  );
}

function eqToken(token, tokenCursorString, message) {
  var split = tokenCursorString.split('|');
  deepEqual({
    // Copy over the attrs we care about (i.e., exclude toString()).
    value: token.value,
    prefix: token.prefix,
    suffix: token.suffix
  }, {
    value: split.join(''),
    prefix: split[0],
    suffix: split[1] || ''
  }, message ? message : undefined);
}

function eqReplaceAll(cursor, before, replacement, after, message) {
  eqReplaceEl(
    cursor, document.querySelector('#qunit-fixture textarea'),
    before, replacement, after, message
  );
  eqReplaceEl(
    cursor, document.querySelector('#qunit-fixture input'),
    before, replacement, after, message
  );
  strictEqual(
    cursor.replace(parse(before).value, parse(before).index, replacement),
    parse(after).value,
    message ? message + ' (string)' : undefined
  );
}

function eqReplaceEl(cursor, el, before, replacement, after, message) {
  var tag = el.tagName.toLowerCase();
  type(el, before);
  strictEqual(
    cursor.replace(el, replacement), before !== after,
    message ? message + ' (replacement made in ' + tag + '?)' : undefined
  );
  strictEqual(
    el.value, parse(after).value,
    message ? message + ' (' + tag + ' value)' : undefined
  );
  strictEqual(
    el.selectionStart, parse(after).index,
    message ? message + ' (' + tag + ' cursor index)' : undefined
  );
}

function type(el, cursorString) {
  var result = parse(cursorString);
  el.value = result.value;
  el.setSelectionRange(result.index, result.index);
}

test('type', function () {
  var textarea = document.querySelector('#qunit-fixture textarea'),
      input = document.querySelector('#qunit-fixture input');

  type(textarea, 'foo| bar');
  strictEqual(textarea.value, 'foo bar', 'textarea value');
  strictEqual(textarea.selectionStart, 3, 'textarea cusor position');

  type(input, 'foo| bar');
  strictEqual(input.value, 'foo bar', 'input value');
  strictEqual(input.selectionStart, 3, 'input cusor position');
});

function parse(cursorString) {
  var split = cursorString.split('|');
  return { value: split.join(''), index: split[0].length };
}

test('parse', function () {
  deepEqual(parse('|foo bar'), { value: 'foo bar', index: 0 }, 'start');
  deepEqual(parse('foo| bar'), { value: 'foo bar', index: 3 }, 'middle');
  deepEqual(parse('foo bar|'), { value: 'foo bar', index: 7 }, 'end');
});
